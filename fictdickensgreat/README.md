# Texts of [semi]famous fiction

Text is obtained from copyright and royalty free sources such as gutenberg

## Split - arbitrary - a guide and example

Regardless of the which of xxd or hexdump you use, the first thing you will have to do if you want to 
conform to the input decimal range 100 to 470 is to ...

split the text into pieces

With reading text this can be done arbitrarily as the reader will be able to piece together with no special assistance

This assumes a human presence in the information assurance chain, but should not be hard to introduce instead a split and reconstruct routine instead.

more fictdickensgreat1867edition1pageextractLINE1xxd.txt  

```
4a6f65e280997320666f7267652061646a6f696e6564206f757220686f75
73652c20776869636820776173206120776f6f64656e20686f7573652c20
6173206d616e79206f6620746865206477656c6c696e677320696e206f75
7220636f756e74727920776572652ce280946d6f7374206f66207468656d
2c20617420746861742074696d652e205768656e20492072616e20686f6d
652066726f6d2074686520636875726368796172642c2074686520666f72
```

Above is the first 6 lines of LINE1 extract.

The output is paired okay allowing simple concatenation, which is how I will proceed for this example.

6 of those hex lines into a single piece of input would go [usually] but you would have to convert to decimal and check the length

Use 5 or 4 is you want to skip the need to check (assuming semi-random data ... which this is)

I choose 4 and so construct a hex as follows

```
4a6f65e280997320666f7267652061646a6f696e6564206f757220686f75
73652c20776869636820776173206120776f6f64656e20686f7573652c20
6173206d616e79206f6620746865206477656c6c696e677320696e206f75
7220636f756e74727920776572652ce280946d6f7374206f66207468656d
```

Becomes for my Python command line interpreter:

```
int4=int('0x4a6f65e280997320666f7267652061646a6f696e6564206f757220686f7573652c20776869636820776173206120776f6f64656e20686f7573652c206173206d616e79206f6620746865206477656c6c696e677320696e206f757220636f756e74727920776572652ce280946d6f7374206f66207468656d',0)
```

A Decimal integer of 289 long is produced beginning 2833569900 and ending 3551116653

100 < 289 < 470 so we are in [length] range

The input hex 0x4a6f65 ... 68656d produces a base64 represention (should >= 90% of the time)
when run through runner_moded.py 1601 0x...

```
Sm9l4oCZcyBmb3JnZSBhZGpvaW5lZCBvdXIgaG91c2UsIHdoaWNoIHdhcyBhIHdvb2RlbiBob3VzZSwgYXMgbWFueSBvZiB0aGUgZHdlbGxpbmdzIGluIG91ciBjb3VudHJ5IHdlcmUs4oCUbW9zdCBvZiB0aGVt
```

```
python3 ./harness_prevalidator.py 0x4a6f65e280997320666f7267652061646a6f696e6564206f757220686f7573652c20776869636820776173206120776f6f64656e20686f7573652c206173206d616e79206f6620746865206477656c6c696e677320696e206f757220636f756e74727920776572652ce280946d6f7374206f66207468656d
version=53 ; passmark=4 ; length valid flag=True says GOOD for input hex0x=0x4a6f65e280997320666f7267652061646a6f696e6564206f757220686f7573652c20776869636820776173206120776f6f64656e20686f7573652c206173206d616e79206f6620746865206477656c6c696e677320696e206f757220636f756e74727920776572652ce280946d6f7374206f66207468656d
```

By pre-validating you reduce the chance of [data] rejection to < 1%. Always pre-validate before attempting to send.

For good input data like Keys that have been preprocessed to remove obvious identifiers, we might use regular or triple or triple trebled [plug] power mode

Here we are plain texting and the hash would ordinarily think of that as poor data (repetition / predictability) and not recommend transmission over a public network.

Lets assume our network is semi-private and we have reasonably powerful hardware at either end, so we will use anti AI mode ver_minor=298

onetime key (inspired by PI but choose your own value/scheme in real life): 1415926535897932  

ver_minor=298  

0x4a6f65e280997320666f7267652061646a6f696e6564206f757220686f7573652c20776869636820776173206120776f6f64656e20686f7573652c206173206d616e79206f6620746865206477656c6c696e677320696e206f757220636f756e74727920776572652ce280946d6f7374206f66207468656d

WANHEX:
```
0x15ffda751779c8491f06bc82c3d1959847a2f4396ba0c462ecd47fa92166771790e1a141472794bed392bcbda8cfd76ba2351b64c8dd6e7a916366600e9531b364b77a0cf84c183f661ebc67a6b94fc3657de491dc430dd4fbc10a5d30553df87ffb5e62c120769d3256b3fcf5ac91315e0bef0749f13d3e2a5bdbb14c4a87a4cbdb0a119ffce4af55f9e63ef3c25c2248dcd9192602d8c87881b0c4d0366be1923595c13addfc30ccdb12642d3d9da09de1e3805a77dbc0875d1f0f7c6493c7ac02f4f26df4530d3cb6ab1b2904a422358a055dedd1d2ee7391d2e8be390977d02e6f095ecbb5c2382bd019ec69a973fc058a5cad17ee5084312ccab588992c33e45444c51970977e6121f8d125acd3799c2e2eec5d09ce3a63ce9f66f8f11db768ea15f09b868ba81743b31dcb035f555348cd9f03e794c9503512aec2ba6b337e3b58a6bf2ea0a357a2140d91935579cb07dce98f35e51b0f5f568326c5346801143b7853c607aa3f0eb8834489862098a78dca0878d9cdef43c73
```

Sending WANHEX first over the semi-private, followed by the real data over a fully private (using encryption), could be a method of information assurance.

# xxd versus hexdump

The example in the previous section used xxd as it seems easier to match its output to original text in online converters.

73652c20776869636820776173206120776f6f64656e20686f7573652c20 -> se, which was a wooden house, ?

Note: If your platform is not little endian then you might not be [easily] able to reproduce the example above but sed and awk should give you tools to make it so.


## Regenerating LINE1 file yourself - commands to use

```
head -n 1 fictdickensgreat1867edition1pageextract.txt | xxd -p > fictdickensgreat1867edition1pageextractLINE1xxd.txt
```
