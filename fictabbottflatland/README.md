# Texts of [semi]famous fiction

Text is obtained from copyright and royalty free sources such as gutenberg

## Split - arbitrary - a guide and example

Regardless of the which of xxd or hexdump you use, the first thing you will have to do if you want to 
conform to the input decimal range 100 to 470 is to ...

split the text into pieces

With reading text this can be done arbitrarily as the reader will be able to piece together with no special assistance

This assumes a human presence in the information assurance chain, but should not be hard to introduce instead a split and reconstruct routine instead.

more fictabbottflatland1884edition1pageextractLINE1.txt 

```
492063616c6c206f757220776f726c6420466c61746c616e642c206e6f74
20626563617573652077652063616c6c20697420736f2c2062757420746f
206d616b65206974730a6e617475726520636c656172657220746f20796f
752c206d7920686170707920726561646572732c2077686f206172652070
726976696c6567656420746f206c69766520696e0a53706163652e0a
```

Above is the first 5 lines of LINE1 extract.

The output is paired okay allowing simple concatenation, which is how I will proceed for this example.

5 of those hex lines into a single piece of input would go [usually] but you would have to convert to decimal and check the length

Use 3 or 4 is you want to skip the need to check (assuming semi-random data ... which this is)

I choose 4 and so construct a hex as follows

```
492063616c6c206f757220776f726c6420466c61746c616e642c206e6f74
20626563617573652077652063616c6c20697420736f2c2062757420746f
206d616b65206974730a6e617475726520636c656172657220746f20796f
752c206d7920686170707920726561646572732c2077686f206172652070
```

Becomes for my Python command line interpreter:

```
int4=int('0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070',0)
```

A Decimal integer of 289 long is produced beginning 2783753379 and ending 8747327600

100 < 289 < 470 so we are in [length] range

The input hex 0x49206361 ... 72652070 produces a base64 represention (should >= 90% of the time)
when run through ./runner_moded.py 1601 0x...

```
SSBjYWxsIG91ciB3b3JsZCBGbGF0bGFuZCwgbm90IGJlY2F1c2Ugd2UgY2FsbCBpdCBzbywgYnV0IHRvIG1ha2UgaXRzCm5hdHVyZSBjbGVhcmVyIHRvIHlvdSwgbXkgaGFwcHkgcmVhZGVycywgd2hvIGFyZSBw
```

```
python3 ./harness_prevalidator.py 0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070
version=53 ; passmark=4 ; length valid flag=True says GOOD for input hex0x=0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070
```

For good input data like Keys that have been preprocessed to remove obvious identifiers, we might use regular or triple or triple trebled [plug] power mode

Here we are plain texting and the hash would ordinarily think of that as poor data (repetition / predictability) and not recommend transmission over a public network.

Lets assume our network is semi-private and we have reasonably powerful hardware at either end, so we will use anti AI mode ver_minor=298

onetime key (inspired by PI but choose your own value/scheme in real life): 141591415914159

ver_minor=298  

0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070

WANHEX:
```
0x4c32be1173d299ad9af2e6462f5867f729735d321703b11bd2a7a7ebb158db30d597a25f39615e60ccfd55205171707ba0707e787ae3de2b4fc793b1c2d8874a7dcc090da1d7dd50e1c5e076fbb280a58ad62447554b62bc072f21fda9ea92c36ed7b5ee620f259ff31dbf40d9eb13726138c294a40d5c76331e7108987e7711170150df3f057f40d447621826182c715c7c627a1c9712025612da9880a130e6e8c27635e108beada280ba485067c0658e54f08f837150cc52f61aa509414359ce712f7d93c8a38260da9d7998e2253b049ce3afacf2da7c052d9900669b81c39f3efc689a48679eca5a05f258318bbc15e60ac51a62dbf5537b6de3de96b416bce23811a22fcc3a2547f1bba7f82fc8e940f8344e79f4cffa6ee087f8e021df806d450acc3ccb7b7ecf19b586e7b47f34510743879170b7c6f7466d5379abfc1ee6efa72e21816d4a9f93c4dad5ef9d74e798527d52f8351103bccfb0112ca126437fda145e4786c82ac067143e30c96974e04fd9a19211d8ead703
```

Sending WANHEX first over the semi-private, followed by the real data over a fully private (using encryption), could be a method of information assurance.

# xxd versus hexdump

The example in the previous section used xxd as it seems easier to match its output to original text in online converters.

492063616c6c206f757220776f726c6420466c61746c616e642c206e6f74 -> I call our world Flatland, not

Note: If your platform is not little endian then you might not be [easily] able to reproduce the example above but sed and awk should give you tools to make it so.


## Failure to produce enough entropy when converted to base64 - approaches to mitigate / work around

The pre-validator is there to be used to catch these situations which occur in less than 10% of input cases and depend on specific hex edge cases

So you fail the pre-validator and the output from it does NOT say GOOD ...  
...what to do next?

shrink or extend your chunk

Look again at our earlier example [which I repeat below] and pretend it failed to process.

more fictabbottflatland1884edition1pageextractLINE1.txt 

```
492063616c6c206f757220776f726c6420466c61746c616e642c206e6f74
20626563617573652077652063616c6c20697420736f2c2062757420746f
206d616b65206974730a6e617475726520636c656172657220746f20796f
752c206d7920686170707920726561646572732c2077686f206172652070
726976696c6567656420746f206c69766520696e0a53706163652e0a
```

We originally produced a chunk by taking the first 4 lines of hex in a concat.

If it failed we would try first 5 [extend], then 2 and 3 [shrink]. Lets extend...

```
int4=int('0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070726976696c6567656420746f206c69766520696e0a53706163652e0a',0)
```

```
%1 = 75049842644665090537965452031915810730436455830817514966477454747407570545930662492678547900923601986033958459923996931132911670894636532792695744864267007103553932938456132577381646549173145163654546513773891189525797754099963091176835887820299650518879281639500317553402235156948408028433507694340308078633188432229276984417371225631880609543104152022538
? length(Str(int4))
%2 = 356
```

So now our chunk is a 356 digit integer rather than the shorter 289 long integer in the shorter chunk from our earlier work

100 < 356 < 470 so we are in [length] range

Use the prevalidator now that we know we have not busted the upper length limits by extending

```
python3 ./harness_prevalidator.py 0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070726976696c6567656420746f206c69766520696e0a53706163652e0a
version=53 ; passmark=4 ; length valid flag=True says GOOD for input hex0x=0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070726976696c6567656420746f206c69766520696e0a53706163652e0a
```

Good to go with the larger chunk so lets do that ...

onetime key (inspired by PI but choose your own value/scheme in real life): 141591415914159

ver_minor=298  

0x492063616c6c206f757220776f726c6420466c61746c616e642c206e6f7420626563617573652077652063616c6c20697420736f2c2062757420746f206d616b65206974730a6e617475726520636c656172657220746f20796f752c206d7920686170707920726561646572732c2077686f206172652070726976696c6567656420746f206c69766520696e0a53706163652e0a

WANHEX:
```
0xede6979d88cf6f091f4dce42b2e5a539b4b450b5eebb4b455417a99f2bd37c4a43b1a58c9adefc009a577a80e631fea7bfcfccaaf4f28490770528856d35de15839db88617ad0dc7a28bafd0f10d235189cd190a47bd04e4774bd2f7aa2838480431f8c82ed94a282707e90f078fe53f70050f63026b05478bb8ddc53505937b84963a3835e5b6fbc8c45afa5fca7591e5b1608dcda29f089f4e320ce2bbb460ace0514f69dde961e6a86e49a3b0233c3d7c79c53e9f9ff63f07f1a76e302af7e7081bbee101dd663dd2552106eeba9d0c6e056328a0823c92c7900ce8ba8850394de301041633759f713a3585048479e8d518f4a7cdc7ac072ee38f33e4512a48092c1e5d53c8a10e57e0c9d746e3354cf3c7a57cd69e5d1be60c98c48de2b0620ee7005141e517625139a8b7667e46dce18c12f8126c2282cd5da9161c2484f20c60e9de563e45f6640f8955a30852849026b8fdba54ba77f05caea1acf9621da464c55a78c76540173aa0d8d1690f75f34e4fef058d23889ee49f
```

We have demonstrated here how to adapt to the rare cases where your hex data either does not transfer to valid base64 or has terrible entropy once transferred.

Summary of action: extend or shrink your chunk and push through the new data.

