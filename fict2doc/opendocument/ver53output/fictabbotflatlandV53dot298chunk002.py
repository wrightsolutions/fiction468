#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause


import base64
from datetime import datetime as dt
from os import linesep,path
from sys import path as syspath
import time
from sys import exit

syspath.append(path.join(path.dirname(__file__), "lib"))
try:
    #import prsal
    from prsal import sal1520onetime as sha   # sha mnemonic for sal harness 
except ImportError:
    print(syspath)
    raise

try:
	from sal1520onetime import OnetimeDec,Onetime16,EndFeed
	from onetimethread import OnetimeThread
except ImportError:
	from prsal.sal1520onetime import OnetimeDec,Onetime16,EndFeed
	from prsal.onetimethread import OnetimeThread

try:
	from dechext import DecHext,Dzz,D43zz,CounterPlus,CounterAlpha16
	from listex import Cons,Cons4,Pow16
except ImportError:
	from prsal.dechext import DecHext,Dzz,D43zz,CounterPlus,CounterAlpha16
	from prsal.listex import Cons,Cons4,Pow16


THREE=3		# Because everything in here should be using same threadcount=3, lets have a convenience var
""" Next is the power of the pr sal number we are using as MODULO in operations
SALTY is just a mnemonic for sal number t
"""
SALTY=1520

vermaj=Cons.VER53
vermin=298			# 298==99+66+33 and an extra outer varper4
print("{0} Harness for flatland running version {1}.{2}".format(dt.now().isoformat(),vermaj,vermin))
hex0x='0x6f67726573736261722f504b0304140000080000d3995b530000000000000000000000001a000000436f6e66696775726174696f6e73322f7374617475736261722f504b0304140000080000d3995b5300000000000000000000000018000000436f6e66696775726174696f6e73322f746f6f6c6261722f504b0304140000080000d3995b5300000000000000000000000018000000'
ot3 = Onetime16(SALTY,THREE,hex0x)			# Instanciate a Onetime object which will handle our end2end attempt
print("dec:{0}".format(ot3.inputdec))
otk_pi=1908190819081002
endfeed = ot3.end2end2(otk_pi,vermaj,ver_minor=vermin,verbosity=2)		# ver_minor=298 is one of ai targetted modes
# Because verbosity set high ( 2 or greater ), our call returns an EndFeedInsecure that has extra fields we want.
if endfeed.feedback is not None:
	raise ValueError("end2end6 did not complete as expected. Feedback={0}".format(endfeed.feedback))
#final_after_outerdec = endfeed.final_after_outerdec	# internally: aliased to input_after_prc3 then copied to dec_to_ott unchanged
#Above currently None when running ver 42 of t=1520 code base
#print("{0} final_after_outer (as decimal)->{1}".format(dt.now().isoformat(),final_after_outerdec))
"""
# Next report on the final result after both outer and inner operations.
wanmpz = endfeed.wanmpz
print("wanmpz->{0}".format(wanmpz))
final64 = endfeed.final64
print("final64->{0}".format(final64))
"""
finaldec = endfeed.finaldec
print("finaldec->{0}".format(finaldec))
print("Outer will now be dealt with starting at final_after_outerdec.")
# Next line [assert] is your vital check. Make a similar check in any harness you create where demonstrating reversibility is important.
#assert hex0x_recovered_by_inverting == Cons4.TERMITES48X
""" So far we have demonstrated that the first part (pre the large matrix) can be inverted taking final_after_outerdec back to input [decimal]
Next we concentrate on the matrix. We will have supplied it with value final_after_outerdec and a 10 tuple of powers to use.
We aim to demonstrate that final_after_outerdec equals finaldec*M^-1
"""
mat_inverse = int(endfeed.prod10inv)		# M^-1 as an integer. Retrieved from field prod10inv of the EndFeed [subclass] object
""" Signature for OnetimeThread is (salty, sal, threadcount, j, k, p, s, t, u, v, w, x, z, inputdec=0)
Typical end2end internal creation: OnetimeThread(OnetimeDec.SALTY,None,self.threadcount,jpow,kpow,ppow,spow,tpow,upow,vpow,wpow,xpow,zpow) as sal3:
We are only instanciating a OnetimeThread here to make use of ott.modded_product_of_supplied(iterable_given)
That saves us constructing the pr [sal] number in here to do the modding.
"""
list16 = [0] * 16
ONE=1
p16zeros = Pow16(list16)
ott = OnetimeThread(SALTY,None,ONE,p16zeros)		# salty=683,sal=none,threadcount=1,...
mat_decimal_input_recovered = ott.modded_product_of_supplied([finaldec,mat_inverse])
print("dec_to_ott reconstruction mat_decimal_input_recovered={0}".format(mat_decimal_input_recovered))
mat_decimal_input_unprotected = ott.unprotectdec(val=mat_decimal_input_recovered,verbosity=0)
# TERMITES48*4*dp is 4*dp*???? == TODO
protectdec=endfeed.before16
ottdec_recovered=OnetimeThread.unprotect_decgiven(protectdec)
print("Before onetimethread [recovered]={0}".format(ottdec_recovered))
assert mat_decimal_input_unprotected == ottdec_recovered
#dhr = DecHext(ottdec_recovered)
dhr = D43zz(ottdec_recovered)
print("base64str1_after_inverses={0}".format(dhr.base64str1))
exit()
